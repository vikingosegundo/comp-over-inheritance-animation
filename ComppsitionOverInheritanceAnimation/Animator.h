//
//  Animator.h
//  ComppsitionOverInheritanceAnimation
//
//  Created by Manuel Meyer on 22.03.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Animator <NSObject>

-(void)animate;

@end

@interface Animator : NSObject <Animator>
-(instancetype)initWithView:(UIView *)view;
@end


@interface FadeInAndOutAnimator : Animator
-(instancetype)initWithView:(UIView *)view
             fadeInDuration:(CGFloat)fadeInDuration
            fadeOutDuration:(CGFloat)fadeOutDuration;


-(instancetype)initWithView:(UIView *)view
             fadeInDuration:(CGFloat)fadeInDuration
            fadeOutDuration:(CGFloat)fadeOutDuration
                fadeInDelay:(CGFloat) fadeInDelay
                fadeOuDelay:(CGFloat) fadeOutDelay;
@end