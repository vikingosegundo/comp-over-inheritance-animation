//
//  ViewController.m
//  ComppsitionOverInheritanceAnimation
//
//  Created by Manuel Meyer on 22.03.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import "ViewController.h"
#import "Animator.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) id<Animator> animator;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)animateTapped:(id)sender {
    self.animator = [[FadeInAndOutAnimator alloc] initWithView:self.statusView
                                                fadeInDuration:.8
                                               fadeOutDuration:.5];
    [self.animator animate];
}

- (IBAction)animateFastTapped:(id)sender {
    self.animator = [[FadeInAndOutAnimator alloc] initWithView:self.statusView
                                                fadeInDuration:.8
                                               fadeOutDuration:.5
                                                   fadeInDelay:0
                                                   fadeOuDelay:0];
    [self.animator animate];
}
@end
