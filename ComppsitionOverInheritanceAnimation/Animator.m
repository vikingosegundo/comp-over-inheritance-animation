//
//  Animator.m
//  ComppsitionOverInheritanceAnimation
//
//  Created by Manuel Meyer on 22.03.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import "Animator.h"

@interface Animator ()
@property(nonatomic, weak) UIView *view;
@end

@implementation Animator
-(instancetype)initWithView:(UIView *)view
{
    self = [super init];
    if (self){
        self.view = view;
    }
    return self;
}

-(void)animate {
    NSAssert(NO, @"%@ must be overwritten in a subclass", NSStringFromSelector(_cmd));

}
@end


@interface FadeInAndOutAnimator ()
@property CGFloat fadeOutDuration;
@property CGFloat fadeInDuration;
@property CGFloat fadeInDelay;
@property CGFloat fadeOutDelay;
@end

@implementation FadeInAndOutAnimator

-(instancetype)initWithView:(UIView *)view
             fadeInDuration:(CGFloat)fadeInDuration
            fadeOutDuration:(CGFloat)fadeOutDuration
{
    return [self initWithView:view
               fadeInDuration:fadeInDuration
              fadeOutDuration:fadeOutDuration
                  fadeInDelay:.8
                  fadeOuDelay:2.5];
}

-(instancetype)initWithView:(UIView *)view
             fadeInDuration:(CGFloat)fadeInDuration
            fadeOutDuration:(CGFloat)fadeOutDuration
                fadeInDelay:(CGFloat)fadeInDelay
                fadeOuDelay:(CGFloat)fadeOutDelay
{
    self = [super initWithView:view];
    if(self){
        self.fadeOutDuration = fadeOutDuration;
        self.fadeInDuration = fadeInDuration;
        self.fadeInDelay = fadeInDelay;
        self.fadeOutDelay = fadeOutDelay;
    }
    return self;
}


-(void)animate
{
    
    [UIView animateWithDuration:self.fadeInDuration delay:self.fadeInDelay options:0 animations: ^{
        self.view.alpha = 1.0f;
    }
                     completion: ^(BOOL finished) {
                         [UIView animateWithDuration:self.fadeOutDuration delay:self.fadeOutDelay  options:0 animations: ^{
                             self.view.alpha = 0.0f;
                         }
                                          completion: ^(BOOL finished) {
                                              
                                          }];
                     }];
    
}

@end