//
//  main.m
//  ComppsitionOverInheritanceAnimation
//
//  Created by Manuel Meyer on 22.03.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
